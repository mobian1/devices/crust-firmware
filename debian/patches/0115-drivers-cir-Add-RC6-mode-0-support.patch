From 2a264508b5a70d81aa60e693a6a745021dc8de41 Mon Sep 17 00:00:00 2001
From: Jernej Skrabec <jernej.skrabec@siol.net>
Date: Tue, 19 Jan 2021 17:58:50 +0100
Subject: [PATCH 115/166] drivers: cir: Add RC6 mode 0 support

RC6 has two major modes of operation - mode 0 and 6A. Mode 0 has fixed
16 bit code (8 bits for address and 8 bits for command), while 6A mode
can have anything from 16 to 128 bits. In practice, 6A mode most of the
time transfers 32 bits. Also toggle bit in 6A mode is manufacturer
specific.

Implement mode 0 support and clear toggle bit only if manufacturer is
MCE.

Signed-off-by: Jernej Skrabec <jernej.skrabec@siol.net>
Signed-off-by: Samuel Holland <samuel@sholland.org>
---
 drivers/cir/rc6.c | 37 +++++++++++++++++++++++++------------
 1 file changed, 25 insertions(+), 12 deletions(-)

diff --git a/drivers/cir/rc6.c b/drivers/cir/rc6.c
index 7a93bda..4a2a383 100644
--- a/drivers/cir/rc6.c
+++ b/drivers/cir/rc6.c
@@ -10,12 +10,18 @@
 
 #include "cir.h"
 
-#define NUM_DATA_BITS    32
-#define NUM_HEADER_BITS  4
+#define NUM_HEADER_BITS       4
+#define NUM_MODE_0_DATA_BITS  16
+#define NUM_MODE_6A_DATA_BITS 32
+
+#define RC6_MAN_MASK          GENMASK(31, 16)
+#define RC6_MAN_MCE           0x800f0000
+
+#define RC6_MODE(header)      ((header) & 7)
 
 /* RC6 time unit is 16 periods @ 36 kHz, ~444 us. */
-#define RC6_CARRIER_FREQ 36000
-#define RC6_UNIT_RATE    (RC6_CARRIER_FREQ / 16)
+#define RC6_CARRIER_FREQ      36000
+#define RC6_UNIT_RATE         (RC6_CARRIER_FREQ / 16)
 
 /* Convert specified number of time units to number of clock cycles. */
 #define RC6_UNITS_TO_CLKS(num) \
@@ -75,7 +81,7 @@ cir_decode(struct cir_dec_ctx *ctx)
 		if (!ctx->pulse)
 			break;
 		/* Found a leader mark. Initialize the context. */
-		ctx->bits   = 0;
+		ctx->bits   = NUM_HEADER_BITS;
 		ctx->buffer = 0;
 		ctx->state  = RC6_LEADER_S;
 		break;
@@ -85,7 +91,7 @@ cir_decode(struct cir_dec_ctx *ctx)
 		break;
 	case RC6_HEADER_P:
 	case RC6_DATA_P:
-		ctx->bits++;
+		ctx->bits--;
 		ctx->buffer = ctx->buffer << 1 | ctx->pulse;
 		ctx->state++;
 		break;
@@ -93,9 +99,11 @@ cir_decode(struct cir_dec_ctx *ctx)
 		/* This pulse must negate the previous pulse. */
 		if (ctx->pulse == (ctx->buffer & 1)) {
 			ctx->state = RC6_IDLE;
-		} else if (ctx->bits == NUM_HEADER_BITS) {
+		} else if (ctx->bits == 0) {
 			/* Reinitialize the buffer for decoding data. */
-			ctx->bits   = 0;
+			ctx->bits = RC6_MODE(ctx->buffer) == 6 ?
+			            NUM_MODE_6A_DATA_BITS :
+			            NUM_MODE_0_DATA_BITS;
 			ctx->buffer = 0;
 			ctx->state  = RC6_TRAILER_P;
 		} else {
@@ -110,11 +118,16 @@ cir_decode(struct cir_dec_ctx *ctx)
 		/* This pulse must negate the previous pulse. */
 		if (ctx->pulse == (ctx->buffer & 1)) {
 			ctx->state = RC6_IDLE;
-		} else if (ctx->bits == NUM_DATA_BITS) {
+		} else if (ctx->bits == 0) {
+			uint32_t code = ctx->buffer;
+
 			ctx->state = RC6_IDLE;
-			/* Return the scan code minus the toggle bit. */
-			debug("RC6 code %08x", ctx->buffer);
-			return ctx->buffer & ~BIT(15);
+
+			/* Remove MCE toggle bit. */
+			if ((code & RC6_MAN_MASK) == RC6_MAN_MCE)
+				code &= ~BIT(15);
+			debug("RC6 code %08x", code);
+			return code;
 		} else {
 			ctx->state = RC6_DATA_P;
 		}
-- 
2.30.2

