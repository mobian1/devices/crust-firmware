From 819a8e4f9b6b0e24367156aa5398f1d82b67404a Mon Sep 17 00:00:00 2001
From: Samuel Holland <samuel@sholland.org>
Date: Sat, 9 Jan 2021 18:17:29 -0600
Subject: [PATCH 104/166] drivers: sunxi-cir: Implement proper probe/release
 functions

When first adding CIR support, I attempted to implement it in a way that
did not require any changes to Linux. However, that made integration
with the reference-counting driver classes problematic. The CIR driver
needs setup at suspend time (to switch clocks), but could not be left in
reset at resume time. The reference-counting model leaves only three
possibilities, all unsatisfactory:
 - Run both probe and release (breaks Linux if its driver is loaded)
 - Run neither function (breaks Crust if the Linux driver is loaded)
 - Leak references (eventually crashes Crust)

The only satisfactory approach is the one taken already for I2C and RSB:
add suspend/resume hooks to the Linux driver so it expects the device to
be in reset when resuming. Then the usual Crust probe/release function
structure can be used.

Thankfully, this has little impact when running with an unpatched Linux.
The only problem is that no CIR events will be received after resume.
since the driver only access registers from inside the interrupt
handler (which will never run), nothing else breaks.

Signed-off-by: Samuel Holland <samuel@sholland.org>
---
 drivers/cir/sunxi-cir.c | 44 ++++++++++++++++++++++++-----------------
 1 file changed, 26 insertions(+), 18 deletions(-)

diff --git a/drivers/cir/sunxi-cir.c b/drivers/cir/sunxi-cir.c
index b7e1eb3..592719a 100644
--- a/drivers/cir/sunxi-cir.c
+++ b/drivers/cir/sunxi-cir.c
@@ -24,9 +24,6 @@
 struct sunxi_cir_state {
 	struct device_state ds;
 	struct rc6_ctx      rc6_ctx;
-	uint32_t            clk_stash;
-	uint32_t            cfg_stash;
-	uint32_t            ctl_stash;
 };
 
 static inline const struct sunxi_cir *
@@ -63,34 +60,45 @@ sunxi_cir_poll(const struct device *dev)
 }
 
 static int
-sunxi_cir_probe(const struct device *dev UNUSED)
+sunxi_cir_probe(const struct device *dev)
 {
-	const struct sunxi_cir *self  = to_sunxi_cir(dev);
-	struct sunxi_cir_state *state = sunxi_cir_state_for(dev);
+	const struct sunxi_cir *self = to_sunxi_cir(dev);
+	int err;
 
-	state->clk_stash = mmio_read_32(R_CIR_RX_CLK_REG);
-	mmio_write_32(R_CIR_RX_CLK_REG, 0x80000000);
+	/* Set module clock parent and divider. */
+	mmio_write_32(R_CIR_RX_CLK_REG, 0x0);
 
-	state->cfg_stash = mmio_read_32(self->regs + CIR_RXCFG);
+	if ((err = clock_get(&self->bus_clock)))
+		return err;
+	if ((err = clock_get(&self->mod_clock)))
+		goto err_put_bus_clock;
+	if ((err = gpio_get(&self->pin)))
+		goto err_put_mod_clock;
+
+	/* Configure thresholds and sample clock. */
 	mmio_write_32(self->regs + CIR_RXCFG, 0x010f0310);
 
-	state->ctl_stash = mmio_read_32(self->regs + CIR_RXCTL);
-	mmio_write_32(self->regs + CIR_RXCTL, 0x30);
+	/* Enable CIR module. */
 	mmio_write_32(self->regs + CIR_RXCTL, 0x33);
 
 	return SUCCESS;
+
+err_put_mod_clock:
+	clock_put(&self->mod_clock);
+err_put_bus_clock:
+	clock_put(&self->bus_clock);
+
+	return err;
 }
 
 static void
-sunxi_cir_release(const struct device *dev UNUSED)
+sunxi_cir_release(const struct device *dev)
 {
-	const struct sunxi_cir *self  = to_sunxi_cir(dev);
-	struct sunxi_cir_state *state = sunxi_cir_state_for(dev);
+	const struct sunxi_cir *self = to_sunxi_cir(dev);
 
-	mmio_write_32(R_CIR_RX_CLK_REG, state->clk_stash);
-	mmio_write_32(self->regs + CIR_RXCFG, state->cfg_stash);
-	mmio_write_32(self->regs + CIR_RXCTL, 0x30);
-	mmio_write_32(self->regs + CIR_RXCTL, state->ctl_stash);
+	gpio_put(&self->pin);
+	clock_put(&self->mod_clock);
+	clock_put(&self->bus_clock);
 }
 
 static const struct driver sunxi_cir_driver = {
-- 
2.30.2

