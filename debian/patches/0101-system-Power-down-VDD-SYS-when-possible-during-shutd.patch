From b6b6c2a454af8827f8567024b8b82a050469c88b Mon Sep 17 00:00:00 2001
From: Samuel Holland <samuel@sholland.org>
Date: Sat, 9 Jan 2021 00:06:16 -0600
Subject: [PATCH 101/166] system: Power down VDD-SYS when possible during
 shutdown

This is only possible at the maximum suspend depth.

Signed-off-by: Samuel Holland <samuel@sholland.org>
---
 common/regulator_list.c         | 14 ++++++++++++++
 common/system.c                 | 10 ++++++++--
 include/common/regulator_list.h |  5 +++++
 3 files changed, 27 insertions(+), 2 deletions(-)

diff --git a/common/regulator_list.c b/common/regulator_list.c
index 0062192..6a89b07 100644
--- a/common/regulator_list.c
+++ b/common/regulator_list.c
@@ -39,3 +39,17 @@ const struct regulator_handle dram_supply = {
 	.dev = NULL,
 #endif
 };
+
+const struct regulator_handle vdd_sys_supply = {
+#if CONFIG(REGULATOR_AXP803)
+	.dev = &axp803_regulator.dev,
+	.id  = AXP803_REGL_DCDC6,
+#elif CONFIG(REGULATOR_AXP805)
+	.dev = &axp805_regulator.dev,
+	.id  = AXP805_REGL_DCDCD,
+#elif CONFIG(REGULATOR_GPIO_VDD_SYS)
+	.dev = &gpio_vdd_sys_regulator.dev,
+#else
+	.dev = NULL,
+#endif
+};
diff --git a/common/system.c b/common/system.c
index 5ede9ee..f564875 100644
--- a/common/system.c
+++ b/common/system.c
@@ -194,8 +194,11 @@ system_state_machine(uint32_t exception)
 
 			/* Turn off all unnecessary power domains. */
 			regulator_disable(&cpu_supply);
-			if (system_state == SS_SHUTDOWN)
+			if (system_state == SS_SHUTDOWN) {
 				regulator_disable(&dram_supply);
+				if (suspend_depth >= SD_VDD_SYS)
+					regulator_disable(&vdd_sys_supply);
+			}
 
 			/*
 			 * The regulator provider is often part of the same
@@ -229,8 +232,11 @@ system_state_machine(uint32_t exception)
 			 * If it fails, manually turn the regulators back on.
 			 */
 			if (!(pmic = pmic_get()) || pmic_resume(pmic)) {
+				if (system_state == SS_PRE_RESET) {
+					regulator_enable(&vdd_sys_supply);
+					regulator_enable(&dram_supply);
+				}
 				regulator_enable(&cpu_supply);
-				regulator_enable(&dram_supply);
 			}
 			device_put(pmic);
 
diff --git a/include/common/regulator_list.h b/include/common/regulator_list.h
index 84ed69a..b7d5f40 100644
--- a/include/common/regulator_list.h
+++ b/include/common/regulator_list.h
@@ -18,4 +18,9 @@ extern const struct regulator_handle cpu_supply;
  */
 extern const struct regulator_handle dram_supply;
 
+/**
+ * The regulator supplying VDD-SYS.
+ */
+extern const struct regulator_handle vdd_sys_supply;
+
 #endif /* COMMON_REGULATOR_LIST_H */
-- 
2.30.2

