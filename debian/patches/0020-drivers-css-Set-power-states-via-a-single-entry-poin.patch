From 7258eb3417f33ba5dc3639adab2812b1c549f755 Mon Sep 17 00:00:00 2001
From: Samuel Holland <samuel@sholland.org>
Date: Mon, 23 Nov 2020 23:17:17 -0600
Subject: [PATCH 020/166] drivers: css: Set power states via a single entry
 point

Using a single entry point deduplicates some error checking code, and
gives the driver more freedom about what information it can cache.

Signed-off-by: Samuel Holland <samuel@sholland.org>
---
 common/scpi_cmds.c    | 16 ++++------------
 common/system.c       |  5 ++---
 drivers/css/css.c     | 24 ++++++++++++++++++++++++
 include/drivers/css.h | 15 +++++++++++++++
 4 files changed, 45 insertions(+), 15 deletions(-)

diff --git a/common/scpi_cmds.c b/common/scpi_cmds.c
index c614281..7cf5dbe 100644
--- a/common/scpi_cmds.c
+++ b/common/scpi_cmds.c
@@ -104,19 +104,11 @@ scpi_cmd_set_css_power_handler(uint32_t *rx_payload,
 	uint32_t css_state     = bitfield_get(descriptor, 0x10, 4);
 	int err;
 
-	/* Do not check if the CSS should be turned on, as receiving this
-	 * command from an ARM CPU via PSCI implies that it is already on. */
-	if (cluster_state == SCPI_CSS_ON &&
-	    (err = css_set_cluster_state(cluster, cluster_state)))
-		return err;
-	if ((err = css_set_core_state(cluster, core, core_state)))
-		return err;
-	if (cluster_state != SCPI_CSS_ON &&
-	    (err = css_set_cluster_state(cluster, cluster_state)))
-		return err;
-	if (css_state != SCPI_CSS_ON &&
-	    (err = css_set_css_state(css_state)))
+	err = css_set_power_state(cluster, core, core_state,
+	                          cluster_state, css_state);
+	if (err)
 		return err;
+
 	/* Turning everything off means system suspend. */
 	if (css_state == SCPI_CSS_OFF)
 		system_suspend();
diff --git a/common/system.c b/common/system.c
index 2470fb5..3132d71 100644
--- a/common/system.c
+++ b/common/system.c
@@ -219,9 +219,8 @@ system_state_machine(uint32_t exception)
 			mailbox = device_get_or_null(&msgbox.dev);
 
 			/* Resume execution on the first CPU in the CSS. */
-			css_set_css_state(SCPI_CSS_ON);
-			css_set_cluster_state(0, SCPI_CSS_ON);
-			css_set_core_state(0, 0, SCPI_CSS_ON);
+			css_set_power_state(0, 0, SCPI_CSS_ON,
+			                    SCPI_CSS_ON, SCPI_CSS_ON);
 
 			debug("Resume complete!");
 
diff --git a/drivers/css/css.c b/drivers/css/css.c
index 4eaccf6..a5d3480 100644
--- a/drivers/css/css.c
+++ b/drivers/css/css.c
@@ -157,3 +157,27 @@ css_set_core_state(uint32_t cluster, uint32_t core, uint32_t state)
 
 	return SCPI_OK;
 }
+
+int
+css_set_power_state(uint32_t cluster, uint32_t core, uint32_t core_state,
+                    uint32_t cluster_state, uint32_t css_state)
+{
+	int err;
+
+	if (css_state == SCPI_CSS_ON &&
+	    (err = css_set_css_state(css_state)))
+		return err;
+	if (cluster_state == SCPI_CSS_ON &&
+	    (err = css_set_cluster_state(cluster, cluster_state)))
+		return err;
+	if ((err = css_set_core_state(cluster, core, core_state)))
+		return err;
+	if (cluster_state != SCPI_CSS_ON &&
+	    (err = css_set_cluster_state(cluster, cluster_state)))
+		return err;
+	if (css_state != SCPI_CSS_ON &&
+	    (err = css_set_css_state(css_state)))
+		return err;
+
+	return SCPI_OK;
+}
diff --git a/include/drivers/css.h b/include/drivers/css.h
index 5c881b6..d0d6109 100644
--- a/include/drivers/css.h
+++ b/include/drivers/css.h
@@ -87,4 +87,19 @@ int css_set_cluster_state(uint32_t cluster, uint32_t state);
  */
 int css_set_core_state(uint32_t cluster, uint32_t core, uint32_t state);
 
+/**
+ * Set the state of a CPU core and its ancestor power domains. There are no
+ * restrictions on the requested power states; the best available power state
+ * will be computed for each power domain.
+ *
+ * @param cluster       The index of the cluster.
+ * @param core          The index of the core within the cluster.
+ * @param core_state    The requested power state for the core.
+ * @param cluster_state The requested power state for the core's cluster.
+ * @param css_state     The requested power state for the CSS.
+ * @return              An SCPI success or error status.
+ */
+int css_set_power_state(uint32_t cluster, uint32_t core, uint32_t core_state,
+                        uint32_t cluster_state, uint32_t css_state);
+
 #endif /* COMMON_CSS_H */
-- 
2.30.2

