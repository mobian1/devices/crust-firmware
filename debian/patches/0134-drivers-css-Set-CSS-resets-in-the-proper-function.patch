From 02003eaa67e453faa513b70e9f1492732e660899 Mon Sep 17 00:00:00 2001
From: Samuel Holland <samuel@sholland.org>
Date: Sat, 27 Feb 2021 20:44:36 -0600
Subject: [PATCH 134/166] drivers: css: Set CSS resets in the proper function

CPU_SYS_RESET is distinct from the cluster resets even on single-cluster
SoCs. It resets the part of the GIC outside the cluster, and controls
access to the CPU_SYS_CFG MMIO range.

For cluster idle, the GIC must continue functioning while the cluster is
powered down. To do this, the CSS reset must be moved out of the cluster
suspend/resume function and put in its proper place.

Signed-off-by: Samuel Holland <samuel@sholland.org>
---
 drivers/css/sun50i-a64-css.c | 26 ++++++++++++++++++++------
 drivers/css/sun50i-h6-css.c  | 24 ++++++++++++++++++++----
 2 files changed, 40 insertions(+), 10 deletions(-)

diff --git a/drivers/css/sun50i-a64-css.c b/drivers/css/sun50i-a64-css.c
index 474ce94..3e2b6fd 100644
--- a/drivers/css/sun50i-a64-css.c
+++ b/drivers/css/sun50i-a64-css.c
@@ -15,6 +15,26 @@
 /* Reset Vector Base Address. */
 static uint32_t rvba;
 
+void
+css_suspend_css(uint32_t new_state)
+{
+	if (new_state < SCPI_CSS_OFF)
+		return;
+
+	/* Assert the CPU subsystem reset (active-low). */
+	mmio_write_32(CPU_SYS_RESET_REG, 0);
+}
+
+void
+css_resume_css(uint32_t old_state)
+{
+	if (old_state < SCPI_CSS_OFF)
+		return;
+
+	/* Deassert the CPU subsystem reset (active-low). */
+	mmio_write_32(CPU_SYS_RESET_REG, CPU_SYS_RESET);
+}
+
 void
 css_suspend_cluster(uint32_t cluster UNUSED, uint32_t new_state)
 {
@@ -33,9 +53,6 @@ css_suspend_cluster(uint32_t cluster UNUSED, uint32_t new_state)
 	mmio_poll_32(C0_CPU_STATUS_REG, C0_CPU_STATUS_REG_STANDBYWFIL2);
 	/* Assert all cluster resets (active-low). */
 	mmio_write_32(C0_RST_CTRL_REG, 0);
-	/* Assert the CPU subsystem reset (active-low). */
-	mmio_write_32(CPU_SYS_RESET_REG, 0);
-	udelay(1);
 	/* Activate the cluster output clamps. */
 	mmio_set_32(C0_PWROFF_GATING_REG, C0_PWROFF_GATING);
 	/* Remove power from the cluster power domain. */
@@ -53,9 +70,6 @@ css_resume_cluster(uint32_t cluster UNUSED, uint32_t old_state)
 	/* Release the cluster output clamps. */
 	mmio_clr_32(C0_PWROFF_GATING_REG, C0_PWROFF_GATING);
 	udelay(1);
-	/* Deassert the CPU subsystem reset (active-low). */
-	mmio_write_32(CPU_SYS_RESET_REG, CPU_SYS_RESET);
-	udelay(1);
 	/* Deassert DBGPWRDUP for all cores. */
 	mmio_write_32(DBG_REG0, 0);
 	/* Assert all cluster and core resets (active-low). */
diff --git a/drivers/css/sun50i-h6-css.c b/drivers/css/sun50i-h6-css.c
index bb0b3fe..fc91f3a 100644
--- a/drivers/css/sun50i-h6-css.c
+++ b/drivers/css/sun50i-h6-css.c
@@ -13,6 +13,26 @@
 /* Reset Vector Base Address. */
 static uint32_t rvba;
 
+void
+css_suspend_css(uint32_t new_state)
+{
+	if (new_state < SCPI_CSS_OFF)
+		return;
+
+	/* Assert the CPU subsystem reset (active-low). */
+	mmio_write_32(CPU_SYS_RESET_REG, 0);
+}
+
+void
+css_resume_css(uint32_t old_state)
+{
+	if (old_state < SCPI_CSS_OFF)
+		return;
+
+	/* Deassert the CPU subsystem reset (active-low). */
+	mmio_write_32(CPU_SYS_RESET_REG, CPU_SYS_RESET);
+}
+
 void
 css_suspend_cluster(uint32_t cluster UNUSED, uint32_t new_state)
 {
@@ -33,8 +53,6 @@ css_suspend_cluster(uint32_t cluster UNUSED, uint32_t new_state)
 	mmio_write_32(C0_RST_CTRL_REG, 0);
 	/* Assert all power-on resets (active-low). */
 	mmio_write_32(C0_PWRON_RESET_REG, 0);
-	/* Assert the CPU subsystem reset (active-low). */
-	mmio_write_32(CPU_SYS_RESET_REG, 0);
 }
 
 void
@@ -43,8 +61,6 @@ css_resume_cluster(uint32_t cluster UNUSED, uint32_t old_state)
 	if (old_state < SCPI_CSS_OFF)
 		return;
 
-	/* Deassert the CPU subsystem reset (active-low). */
-	mmio_write_32(CPU_SYS_RESET_REG, CPU_SYS_RESET);
 	/* Deassert the cluster hard reset (active-low). */
 	mmio_write_32(C0_PWRON_RESET_REG, C0_PWRON_RESET_REG_nH_RST);
 	/* Deassert DBGPWRDUP for all cores. */
-- 
2.30.2

