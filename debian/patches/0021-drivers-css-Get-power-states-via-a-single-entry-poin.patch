From 6c13ad53382e12704c32e6cdc4f5574dc6e4e059 Mon Sep 17 00:00:00 2001
From: Samuel Holland <samuel@sholland.org>
Date: Fri, 27 Nov 2020 16:06:31 -0600
Subject: [PATCH 021/166] drivers: css: Get power states via a single entry
 point

Using a single entry point deduplicates some error checking code, and
gives the driver more freedom about what information it can cache.

Signed-off-by: Samuel Holland <samuel@sholland.org>
---
 common/scpi_cmds.c    | 10 ++++++++--
 drivers/css/css.c     | 19 +++++++++----------
 include/drivers/css.h | 18 +++++++++++++-----
 3 files changed, 30 insertions(+), 17 deletions(-)

diff --git a/common/scpi_cmds.c b/common/scpi_cmds.c
index 7cf5dbe..532ff15 100644
--- a/common/scpi_cmds.c
+++ b/common/scpi_cmds.c
@@ -130,12 +130,18 @@ scpi_cmd_get_css_power_handler(uint32_t *rx_payload UNUSED,
 {
 	uint32_t clusters = css_get_cluster_count();
 	uint16_t descriptor;
+	int err;
 
 	/* Each cluster has its own power state descriptor. */
 	for (uint32_t i = 0; i < clusters; ++i) {
+		uint32_t state, online_cores;
+
+		if ((err = css_get_power_state(i, &state, &online_cores)))
+			return err;
+
 		descriptor = CLUSTER_ID(i) |
-		             CLUSTER_POWER_STATE(css_get_cluster_state(i)) |
-		             CORE_POWER_STATES(css_get_online_cores(i));
+		             CLUSTER_POWER_STATE(state) |
+		             CORE_POWER_STATES(online_cores);
 		/* Work around the hardware byte swapping, since this is an
 		 * array of elements each aligned to less than 4 bytes. */
 		((uint16_t *)tx_payload)[i ^ 1] = descriptor;
diff --git a/drivers/css/css.c b/drivers/css/css.c
index a5d3480..d86a0ea 100644
--- a/drivers/css/css.c
+++ b/drivers/css/css.c
@@ -81,27 +81,26 @@ css_get_core_state(uint32_t cluster, uint32_t core)
 	return SCPI_CSS_ON;
 }
 
-/*
- * There should usually be no reason to override this weak definition. It
- * correctly implements the algorithm specified in the CSS API using the
- * lower-level core state API. However, this definition is declared weak for
- * consistency with the other functions and flexibility for future platforms.
- */
-uint32_t WEAK
-css_get_online_cores(uint32_t cluster)
+int
+css_get_power_state(uint32_t cluster, uint32_t *cluster_state,
+                    uint32_t *online_cores)
 {
 	uint32_t cores;
 	uint32_t mask = 0;
 
-	assert(cluster < css_get_cluster_count());
+	if (cluster >= css_get_cluster_count())
+		return SCPI_E_PARAM;
+
+	*cluster_state = css_get_cluster_state(cluster);
 
 	cores = css_get_core_count(cluster);
 	for (uint32_t core = 0; core < cores; ++core) {
 		if (css_get_core_state(cluster, core) != SCPI_CSS_OFF)
 			mask |= BIT(core);
 	}
+	*online_cores = mask;
 
-	return mask;
+	return SCPI_OK;
 }
 
 /**
diff --git a/include/drivers/css.h b/include/drivers/css.h
index d0d6109..df3523c 100644
--- a/include/drivers/css.h
+++ b/include/drivers/css.h
@@ -45,13 +45,21 @@ uint32_t css_get_core_count(uint32_t cluster) ATTRIBUTE(const);
 uint32_t css_get_core_state(uint32_t cluster, uint32_t core);
 
 /**
- * Get a bitmask of the states of the cores in a cluster. A zero bit indicates
- * that a core is completely off (i.e. it has no execution context, and must be
- * manually woken up). Any other state is represented by a set bit.
+ * Get the state of a cluster and the cores it contains.
  *
- * @param cluster The index of the cluster.
+ * The state of the cluster is returned in cluster_state.
+ *
+ * A bitmap representing the state of each core in the cluster is returned in
+ * online_cores. A zero bit indicates that a core is completely off (it has no
+ * execution context). Any other state is represented by a set bit.
+ *
+ * @param cluster       The index of the cluster.
+ * @param cluster_state Where to store the cluster state.
+ * @param online_cores  Where to store the bitmap of online cores.
+ * @return              An SCPI success or error status.
  */
-uint32_t css_get_online_cores(uint32_t cluster);
+int css_get_power_state(uint32_t cluster, uint32_t *cluster_state,
+                        uint32_t *online_cores);
 
 /**
  * Initialize the CSS driver, assuming the CSS is already running. Since the
-- 
2.30.2

