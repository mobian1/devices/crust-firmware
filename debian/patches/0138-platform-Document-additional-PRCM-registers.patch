From 6b15c15db7bab845d245fdd8da22d58529434ee6 Mon Sep 17 00:00:00 2001
From: Samuel Holland <samuel@sholland.org>
Date: Sat, 13 Mar 2021 13:31:14 -0600
Subject: [PATCH 138/166] platform: Document additional PRCM registers

For H6 and newer, the BSP references an RTC gate register following the
other PRCM gate registers. This register also exists on older SoCs. The
gate controls register access only, not timekeeping.

SoCs with an integrated analog audio codec control it through a register
in the PRCM. This register is documented in the various user manuals, in
the audio codec section instead of the PRCM section.

Signed-off-by: Samuel Holland <samuel@sholland.org>
---
 platform/a64/include/platform/prcm.h | 11 +++++++++++
 platform/h3/include/platform/prcm.h  | 11 +++++++++++
 2 files changed, 22 insertions(+)

diff --git a/platform/a64/include/platform/prcm.h b/platform/a64/include/platform/prcm.h
index f0ff782..209e423 100644
--- a/platform/a64/include/platform/prcm.h
+++ b/platform/a64/include/platform/prcm.h
@@ -23,6 +23,7 @@
 
 /* See r_ccu driver for bit definitions */
 #define APB0_GATE_REG                     (DEV_R_PRCM + 0x0028)
+#define RTC_GATE_REG                      (DEV_R_PRCM + 0x002c)
 
 /* Documented in A23/A31s manual; all bits are present on A64 */
 #define PLL_CTRL_REG0                     (DEV_R_PRCM + 0x0040)
@@ -69,6 +70,16 @@
 
 #define C0_CPUn_PWR_SWITCH_REG(n)         (DEV_R_PRCM + 0x0140 + 0x04 * (n))
 
+#define ADDA_PR_CFG_REG                   (DEV_R_PRCM + 0x01c0)
+#define ADDA_PR_CFG_REG_RESET             BIT(28)
+#define ADDA_PR_CFG_REG_RW                BIT(24)
+#define ADDA_PR_CFG_REG_ADDR(x)           ((x) << 16)
+#define ADDA_PR_CFG_REG_ADDR_MASK         (0x1f << 16)
+#define ADDA_PR_CFG_REG_WDAT(x)           ((x) << 8)
+#define ADDA_PR_CFG_REG_WDAT_MASK         (0xff << 8)
+#define ADDA_PR_CFG_REG_RDAT(x)           ((x) << 0)
+#define ADDA_PR_CFG_REG_RDAT_MASK         (0xff << 0)
+
 #define PRCM_SEC_SWITCH_REG               (DEV_R_PRCM + 0x01d0)
 #define PRCM_SEC_SWITCH_REG_POWER_SEC     BIT(2)
 #define PRCM_SEC_SWITCH_REG_PLL_SEC       BIT(1)
diff --git a/platform/h3/include/platform/prcm.h b/platform/h3/include/platform/prcm.h
index 88386d4..6e3666d 100644
--- a/platform/h3/include/platform/prcm.h
+++ b/platform/h3/include/platform/prcm.h
@@ -23,6 +23,7 @@
 
 /* See r_ccu driver for bit definitions */
 #define APB0_GATE_REG                     (DEV_R_PRCM + 0x0028)
+#define RTC_GATE_REG                      (DEV_R_PRCM + 0x002c)
 
 /* Documented in A23/A31s manual; all bits are present on H3 */
 #define PLL_CTRL_REG0                     (DEV_R_PRCM + 0x0040)
@@ -67,6 +68,16 @@
 
 #define C0_CPUn_PWR_SWITCH_REG(n)         (DEV_R_PRCM + 0x0140 + 0x04 * (n))
 
+#define ADDA_PR_CFG_REG                   (DEV_R_PRCM + 0x01c0)
+#define ADDA_PR_CFG_REG_RESET             BIT(28)
+#define ADDA_PR_CFG_REG_RW                BIT(24)
+#define ADDA_PR_CFG_REG_ADDR(x)           ((x) << 16)
+#define ADDA_PR_CFG_REG_ADDR_MASK         (0x1f << 16)
+#define ADDA_PR_CFG_REG_WDAT(x)           ((x) << 8)
+#define ADDA_PR_CFG_REG_WDAT_MASK         (0xff << 8)
+#define ADDA_PR_CFG_REG_RDAT(x)           ((x) << 0)
+#define ADDA_PR_CFG_REG_RDAT_MASK         (0xff << 0)
+
 #define PRCM_SEC_SWITCH_REG               (DEV_R_PRCM + 0x01d0)
 #define PRCM_SEC_SWITCH_REG_POWER_SEC     BIT(2)
 #define PRCM_SEC_SWITCH_REG_PLL_SEC       BIT(1)
-- 
2.30.2

